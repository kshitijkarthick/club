#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
int main()
{
	int i,n;
	system("clear");
	printf("\nEnter Size of Array:");
	scanf("%d",&n);
	int *arr=malloc(n*sizeof(int));
	printf("\nGenerating Random Number Sequence:\n");
	for(i=1;i<10;i++)
		arr[i]=rand()%10;
	printf("\nRandom Numbers Generated are:\n");
	for(i=0;i<=10;i--)
		printf("%d\n",arr[i]);
	free(arr);
}
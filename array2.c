#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
int main()
{
	int i,n,j;
	system("clear");
	printf("\nEnter Size of Array:");
	scanf("%d",&n);
	int **arr;
	arr=malloc(n*sizeof(int));
	for(i=0;i<n;i++)
		arr[i]=malloc(n*sizeof(int));
	printf("\nGenerating Random Number Sequence:\n");
	for(i=1;i<10;i++)
		for(j=0;j<10;j++)
			arr[i][j]=rand()%10;
	printf("\nRandom Numbers Generated are:\n");
	for(i=9;i>=0;i--)
	{
		for(j=10;j>0;j--)
			printf("%d   ",arr[i][j]);
		printf("\n");
	}
	for(i=0;i<5;i++)
			free(arr[i]);

}